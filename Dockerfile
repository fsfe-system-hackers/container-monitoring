FROM alpine:3.6
ARG TO_EMAIL=system-hackers@lists.fsfe.org
ARG FROM_EMAIL=monitoring@fsfe.org

MAINTAINER Max Mehl <max.mehl@fsfe.org>

RUN set -x && apk add --update curl bash docker msmtp

RUN apk del --purge ${BUILD_PKGS} && rm -rf /tmp/* /var/tmp/* /var/cache/apk/*

COPY cron.txt /var/spool/cron/crontabs/root

COPY check.sh msmtprc /root/

RUN sed -i s/TO_EMAIL_PLACEHOLDER/$TO_EMAIL/ /root/check.sh
RUN sed -i s/FROM_EMAIL_PLACEHOLDER/$FROM_EMAIL/ /root/msmtprc
RUN touch /root/mon.log

CMD crond -l 2  && : >> /root/mon.log && tail -f /root/mon.log

