# Monitoring

The goal of this service is to monitor all services behind the [reverse
proxy](https://git.fsfe.org/fsfe-system-hackers/reverse-proxy) and docker
containers in general.

## What gets monitored?

A containener is monitored if it meets one of the following criteria:

It has the `VIRTUAL_HOST` environment variable defined. For example, with the
following playbook configuration:

    env:
        VIRTUAL_HOST: mailman-confirm.fsfe.org

It has the `fsfe-monitoring` label. For example, with the following playbook
configuration:

    labels:
        "fsfe-monitoring": "true"


## Checks performed

If the container has the VIRTUAL\_HOST environement variable, the following
checks are performed:

* The container is down (status 'Exited')
* The GET to `https://VIRTUAL_HOST` has a return code == 5xxx

If the container has the fsfe-monitoring:true label, this check is performed:

* The container is down (status 'Exited')

An email is sent if one of those checks is positive.

## How it works?

The container is connected to the Docker daemon with the unix socket shared as a
read-only volume. Then it uses `docker ps` to get a list of containers and
perform the checks described above.

The container uses `msmtp` to send send emails with the configuration in
[msmtprc](msmtprc).

# License

This software is copyright 2018 by the Free Software Foundation Europe e.V. and
licensed under the GPLv3 license. For details see the "LICENSE" file in the top
level directory of https://git.fsfe.org/fsfe-system-hackers/monitoring

