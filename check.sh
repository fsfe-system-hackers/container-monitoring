# Copyright (c) 2018 Free Software Foundation Europe <contact@fsfe.org>
# Author 2018 Vincent Lequertier <vincent@fsfe.org>
# SPDX-License-Identifier: GPL-3.0-or-later


basedir=/root
LOG="${basedir}/mon.log"
exec > >(tee -ia "${LOG}")
exec 2>&1

function send_email {
    service_name=$1
    vhost=$2
    date=`date -u +"%Y-%m-%dT%H:%M:%SZ"`
    echo "[$date] $service_name ($vhost) is down!"
    echo -e "Subject: $service_name ($vhost) down!" | msmtp -C "${basedir}"/msmtprc -a fsfe TO_EMAIL_PLACEHOLDER
}

# Save list of all docker containers separated by lines
ps=$(cat <<-END
$(docker ps -a --format "{{.Names}};{{.Status}};{{.ID}}" | sort)
END
)

while read -r line; do
    name=`echo $line | cut -d";" -f1`
    status=`echo $line | cut -d";" -f2`
    id=`echo $line | cut -d";" -f3`
    vhost=`docker inspect -f "{{range .Config.Env}}{{println .}}{{end}}" ${id} | grep "VIRTUAL_HOST" | cut -d"=" -f2`
    label_monitoring=`docker inspect -f "{{ index .Config.Labels \"fsfe-monitoring\"}}" ${id}`

    # If the container is labeled as fsfe-monitoring: true
    if [ "$label_monitoring" == "true" ]; then
        # Send an email if container has exited
        if [[ $status =~ "Exited" ]]; then
            echo "container $name exited"
            send_email $name $vhost
            continue
        fi
    fi

    # If VIRTUAL_HOST is defined
    if [ "$vhost" != "" ]; then
        # Send an email if container has exited
        if [[ $status =~ "Exited" ]]; then
            echo "container $name exited"
            send_email $name $vhost
            continue
        fi

        # Take the first virtual host if multiple are defined
        if [[ $vhost =~ ',' ]]; then
            vhost=`echo $vhost | cut -d, -f1`
        fi

        # Check for 5xx return code for GET https://$vhost. Retry 3 times, use 5
        # seconds as connection timeout, 10 as the global timeout for the whole
        # HTTP request / response, 30 seconds for all the attempts
        return_code=`curl --retry 3 --connect-timeout 5 --max-time 10 --retry-delay 0 --retry-max-time 30 -sL -w "%{http_code}" "https://${vhost}" -o /dev/null`
        if [[ $return_code =~ 5[0-9][0-9] ]]; then
            send_email $name $vhost
        fi
    fi


done < <(echo "$ps")

